<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemoriaController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\RecursividadController;
use App\Http\Controllers\InterfazController;
use App\Http\Controllers\listaFIFOLIFOController;
use App\Http\Controllers\listaInicioFinController;
use App\Http\Controllers\LoginArrayController;
use App\Http\Controllers\ArreglosController;
use App\Http\Controllers\BurbujaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/memoria', [MemoriaController:: class, 'Memoria']);
Route::get('/apuntadores', [InicioController:: class, "Inicio"]);
Route::get('/recursividad', [RecursividadController:: class, 'Incrementable']);
Route::get('/interfaz', [InterfazController:: class, 'Inicio']);
Route::get('/listaFIFOLIFO', [listaFIFOLIFOController:: class, 'Lista']);
Route::get('/listaInicioFin', [listaInicioFinController:: class, 'Lista']);
Route::get('/Login', [LoginArrayController:: class, 'InicioLogin']);
Route::post('/LoginValidacion', [LoginArrayController:: class, 'LoginValidacion']);
Route::get('/Arreglos', [ArreglosController:: class, 'Arreglos']);
Route::get('/ordenamiento-burbuja', [BurbujaController::class, 'OrdenamientoBurbuja']);