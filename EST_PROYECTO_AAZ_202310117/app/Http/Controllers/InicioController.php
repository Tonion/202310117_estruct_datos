<?php
//Plantilla de controlador creada a partir de un comando de artisan.
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InicioController extends Controller
{
    //Creamos una funcion llamada inicio que es donde guardaremos las variables y funciones del código
    public function Inicio(){
        //La edad la pondremos con un valor establecido, y pondremos que returne el view que creamos de html 
        //con el nombre apuntadores para que tenga cuerpo de pagina y se imprima $Aedad que lo pusimos como que tambien su valor era  26
        $edad = 26;
        $Aedad = & $edad;
        unset($edad);
        return view("apuntadores",["Aedad"=>$Aedad]);
    }
}