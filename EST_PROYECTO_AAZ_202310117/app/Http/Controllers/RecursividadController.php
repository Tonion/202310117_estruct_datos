<?php
//Plantilla de controlador creada a partir de un comando de artisan
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecursividadController extends Controller
{
    //Se crea la función que establece el valor N y el final de éste.
    public function recursividad($Nfin, $N){
        $N += 1;
        if($N<=$Nfin){
            echo $N, '<br>';
            $this->recursividad($Nfin, $N);
        }
    }
    //Se crea la función que establece el límite del incremento de N
    public function Incrementable(){
        $this->recursividad(12, 0);
        if($N=13){
            echo $N, " ( ͡° ͜ʖ ͡°)", '<br>';
        }
        $this->recursividad(68, 13);
        if($N=69){
            echo $N, " ( ͡° ͜ʖ ͡°)", '<br>';
        }
    }
}