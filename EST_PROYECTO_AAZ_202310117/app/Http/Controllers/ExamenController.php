<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExamenController extends Controller
{
    public function ArreglosExamen(){
        $array = [1, 2, 3, 4, 5, 6];
        print_r($array);
        echo "<br>";

        $array[] = 7;
        print_r($array);
        echo "<br>";

        unset($array[2]);
        print_r($array);
    }
}
